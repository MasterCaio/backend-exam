from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from .serializers import UserSerializer
from .models import User
from tasks.models import Task
from tasks.serializers import TaskSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @detail_route()
    def task(self, request, pk=None):
        user = self.get_object()
        tasks = Task.objects.filter(user_id=user.id).distinct()
        tasks_json = TaskSerializer(tasks, many=True)
        return Response(tasks_json.data)
