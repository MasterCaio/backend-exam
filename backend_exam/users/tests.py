from django.test import TestCase
import json
from .models import User
from tasks.models import Task


class UserTest(TestCase):
    url = 'http://localhost:8000/backend_exam/users/'

    def setUp(self):
        self.user1 = User.objects.create(
            name='user_test',
            email='usertest@gmail.com',
            age=22,
            user_type='Admin'
        )

    def as_dict(self):
        return {
            'id': self.user1.id,
            'name': self.user1.name,
            'email': self.user1.email,
            'age': self.user1.age,
            'user_type': self.user1.user_type
        }

    def test_user_object_get(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_user_object_post(self):
        json_test = {
            "name": 'another_user_test',
            "email": 'anotherusertest@gmail.com',
            "age": '23',
            "user_type": 'MNG'
        }
        response = self.client.post(
            self.url,
            json_test,
            content_type='application/json')
        self.assertEqual(response.status_code, 201)

        response = self.client.get(self.url)
        self.assertEqual(len(json.loads(response.content)), 2)

    def test_user_object_read(self):
        self.url += '{}/'.format(self.user1.id)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertAlmostEqual(json.loads(response.content), self.as_dict())

    def test_user_object_update(self):
        self.url += '{}/'.format(self.user1.id)
        response = self.client.put(
            self.url,
            data={},
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)

    def test_user_object_delete(self):
        self.url += '{}/'.format(self.user1.id)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, 204)

        response = self.client.get('http://localhost:8000/backend_exam/users/')
        self.assertEqual(len(json.loads(response.content)), 0)

    def tearDown(self):
        User.objects.all().delete()
        Task.objects.all().delete()
