from django.db import models
from django.core.validators import MinValueValidator


class User(models.Model):

    USER_TYPE_CHOICES = (('ADM', 'Admin'),
                         ('MNG', 'Manager'),
                         ('DFT', 'Default'))

    name = models.CharField(max_length=50)
    email = models.EmailField()
    age = models.PositiveIntegerField(validators=[MinValueValidator(18)])
    user_type = models.CharField(max_length=3, choices=USER_TYPE_CHOICES)
