from django.conf.urls import url, include
from .views import TaskViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)

urlpatterns = [
    url('', include(router.urls)),
]
