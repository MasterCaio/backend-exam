from django.test import TestCase
import json
from .models import Task
from users.models import User


class TaskTest(TestCase):
    url = 'http://localhost:8000/backend_exam/tasks/'

    def setUp(self):
        self.user1 = User.objects.create(
            name='user_test',
            email='usertest@gmail.com',
            age=22,
            user_type='Admin'
        )

        self.task1 = Task.objects.create(
            user_id=self.user1,
            description='task_description'
        )

    def as_dict(self):
        return {
            'user_id': self.task1.user_id.id,
            'description': self.task1.description
        }

    def test_task_object_get(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_task_object_post(self):
        json_test = {
            "user_id": self.user1.id,
            "description": 'new_task'
        }
        response = self.client.post(
            self.url,
            json_test,
            content_type='application/json')
        self.assertEqual(response.status_code, 201)

        response = self.client.get(self.url)
        self.assertEqual(len(json.loads(response.content)), 2)

    def test_task_object_read(self):
        self.url += '{}/'.format(self.task1.id)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_task_object_update(self):
        self.url += '{}/'.format(self.task1.id)
        response = self.client.put(
            self.url,
            data={},
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)

    def test_task_object_delete(self):
        self.url += '{}/'.format(self.task1.id)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, 204)

        response = self.client.get('http://localhost:8000/backend_exam/tasks/')
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_list_tasks_by_user(self):
        self.url = 'http://localhost:8000/backend_exam/users/'
        self.url += '{}/'.format(self.user1.id)
        self.url += 'task/'
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(response.content)), 1)

    def tearDown(self):
        Task.objects.all().delete()
        User.objects.all().delete()
