from django.urls import path, include


urlpatterns = [
    path('backend_exam/', include([
        path('', include('users.urls')),
        path('', include('tasks.urls')),
    ])),
]
