
#### Pre-requisites:
- Python 3.6
- virtualenv

## Steps virtual env:

### 1. In the project root folder
```
$ virtualenv venv
```
### 2. Activate the virtual environment
```
$ source venv/bin/activate
```
### 3. Install project dependencies
```
$ pip install -r requirements.txt
```
### 4. Access project folder
```
$ cd backend_exam
```
### 5. Make migrations
```
$ python manage.py makemigrations
```

### 6. Execute migrations
```
$ python manage.py migrate
```

### 7. Run the application
```
$ python manage.py runserver
```

### 8. Run tests
```
$ python manage.py test
```